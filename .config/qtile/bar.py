import os
import socket
import subprocess

from libqtile.config import Screen
from libqtile.lazy import lazy
from libqtile.bar import Gap, Bar
from libqtile import widget

from colors import colors

screen_width = int(subprocess.Popen('xrandr | grep primary | cut -d" " -f4 | cut -d"+" -f1 | cut -d"x" -f1',shell=True, stdout=subprocess.PIPE).communicate()[0])
screen_height = int(subprocess.Popen('xrandr | grep primary | cut -d" " -f4 | cut -d"+" -f1 | cut -d"x" -f2',shell=True, stdout=subprocess.PIPE).communicate()[0])

size_of_font = int(screen_width/120)
size_of_separator = int(screen_width/320)
size_of_padding = int(screen_width/960)
size_of_bar_height=int(screen_width/73)

widget_defaults = dict(
    font='Iosevka Fixed',
    fontsize=size_of_font,
    padding=size_of_padding,
    foreground = colors[0],
    background=colors[0]
)

def icon_settings():
	return {
		'padding': 0,
		'fontsize': 28
	}

def left_arrow(color1, color2):
	return widget.TextBox(
		text = '\uE0B2',
		background = color1,
		foreground = color2,
		**icon_settings()
	)

def right_arrow(color1, color2):
	return widget.TextBox(
		text = '\uE0B0',
		background = color1,
		foreground = color2,
		**icon_settings()
	)

def init_widget_list(screen_number):
    left = []
    left.extend([
    widget.Sep(padding=size_of_separator, linewidth=0, background=colors[3]),
    widget.TextBox(text="",font="FontAwesome",background=colors[3]) ])

    if(screen_number==0):
        left.append( widget.Prompt(background=colors[3], prompt=' Launch: ' ) )

    left.extend([
    right_arrow(colors[7],colors[3]),
    widget.Sep(padding=size_of_separator, linewidth=0, background=colors[7]),
    widget.GroupBox(
			background=colors[7],
			this_current_screen_border = colors[3],
			this_screen_border = colors[3],
			other_current_screen_border=colors[7],
			other_screen_border=colors[7],
			hide_unused = False,
			rounded = True,
			highlight_method = 'block',
			fontsize = size_of_font,
			font='FontAwesome',
			padding_y=size_of_padding*2,
			active=colors[0],
			inactive=colors[0],
    ),
    right_arrow(colors[0],colors[7]),
    widget.Sep(padding=size_of_separator, linewidth=0, background=colors[0]),
    widget.WindowName(background=colors[0], foreground=colors[3],format='{state}{name}'),
    #right_arrow(colors[3],colors[7]),
    ])

    right = []

    #right.extend([
    #left_arrow(colors[0],colors[3]),
    #widget.TextBox(text=str(size_of_font),background=colors[3])
    #])

    if(screen_number==0):
        right.extend([
            left_arrow(colors[0],colors[3]),
            widget.Systray(background=colors[3]), widget.Sep(padding=size_of_separator,linewidth=0,background=colors[3]),
            left_arrow(colors[3],colors[7])
        ])
    else:
        right.extend([left_arrow(colors[0],colors[7])])


    right.extend([
    widget.TextBox(text="  ",font="FontAwesome",background=colors[7]),
    widget.KeyboardLayout(background=colors[7]),
    widget.Sep(padding=size_of_separator, linewidth=0, background=colors[7]),
    left_arrow(colors[7],colors[3]),
    widget.TextBox(text="  ",font="FontAwesome",background=colors[3]),
    widget.Memory(background=colors[3], measure_mem='M' , format='{MemUsed:.0f}/{MemTotal:.0f} MiB'),
    widget.Sep(padding=size_of_separator,linewidth=0,background=colors[3],),
    left_arrow(colors[3],colors[7]),
    widget.TextBox(text="  ",font="FontAwesome",background=colors[7]),
    widget.CPU(background=colors[7],format='{load_percent:0.1f}%'),
    widget.Sep(padding=size_of_separator,linewidth=0,background=colors[7],),
    left_arrow(colors[7],colors[3]),
    widget.TextBox(text="  ",font="FontAwesome",background=colors[3]),
    widget.Net(background=colors[3],format = '{down:.0f}{down_suffix} ↓↑ {up:.0f}{up_suffix}'),
    widget.Sep(padding=size_of_separator,linewidth=0,background=colors[3],),
    left_arrow(colors[3], colors[7]),
    widget.TextBox(text="  ",font="FontAwesome",background=colors[7]),
    widget.Clock(background=colors[7],format= '%I:%M'),
    widget.Sep(padding=size_of_separator,linewidth=0,background=colors[7],),
    left_arrow(colors[7], colors[3]),
    widget.TextBox(text="  ",font="FontAwesome",background=colors[3]),
    widget.Clock(background=colors[3], format= '%d %b, %A'),
    widget.Sep(padding=size_of_separator,linewidth=0,background=colors[3],),
    ])
    return left+right

def init_screen(screen_number):
	gap = Gap(10)
	return Screen(
        wallpaper='~/.config/wallpapers/clouds1uwu.jpg',
        wallpaper_mode="fill",
		bottom = gap,
		left = gap,
		right = gap,
		top = Bar(
			init_widget_list(screen_number),
			size_of_bar_height,
			#background = colors[0][0],
			opacity = 1,
                        margin = [0, 0, 10 , 0]
		)
	)

screens = [
	init_screen(0),
	init_screen(1)
]
