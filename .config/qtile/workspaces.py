from libqtile.config import Key, Group
from libqtile.lazy import lazy
from config import keys
mod = "mod4"

#UwU



group_names = [("", {'layout': 'stack'}, ["qutebrowser"]),
               ("", {'layout': 'stack'}, []),
               ("", {'layout': 'stack'}, ['steam']),
               ("", {'layout': 'stack'}, ['discord']),
               ("", {'layout': 'stack'}, ['alacritty']),
               ]

groups = [Group(name, **kwargs, spawn=spawn_list) for name, kwargs, spawn_list in group_names]

workspaces = []

#for i, (name, kwargs, spawn_list) in enumerate(group_names, 1):
 #   Key([mod], str(i), lazy.group[name].toscreen())       # Switch to another group
  #  Key([mod, "shift"], str(i), lazy.window.togroup(name)) # Send current window to another group

#for i, group in enumerate(groups, 1):
#	name = str(i % len(group_names))
#	workspaces.extend([
#		Key([mod], name, lazy.group[group.name].toscreen()),
#		Key([mod, "shift"], name, lazy.window.togroup(group.name, switch_group=True)),
#	])

for i, (name, kwargs, spawn_list) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group
