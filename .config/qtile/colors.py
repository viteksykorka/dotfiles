_colors = [
        '1b1c26', # panel bg
        '485062', # current screen tab bg
        'f99db3', # group name text
        'b1b6ff', # focused window border line
        'f984a0', # unfocused windows border line
        'ffffff', # even widgets
        'c6c9ef',
        'f9678a',
        'b2b7ef',
        'bbebca',
]

map_color = lambda c: [f'#{c}', f'#{c}']

colors = tuple(map(map_color, _colors))
