" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin()

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'valloric/youcompleteme'
"Plug 'Townk/vim-autoclose'
Plug 'vim-airline/vim-airline'
"Plug 'dense-analysis/ale'
"Plug 'vim-syntastic/syntastic'

call plug#end()

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
