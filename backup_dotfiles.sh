#!/bin/bash

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

echo "dirname : [$SCRIPTPATH]"

function backupDotFiles() {
    while IFS= read -r ecfg; do
        while IFS= read -r location; do
        echo "Backing up: $location"
        cp ${HOME}/$location ${SCRIPTPATH}/$location
        done < ${SCRIPTPATH}/.config_locations/$ecfg
    done < ${SCRIPTPATH}/.config_locations/enabled_configs
}

function installDotFiles() {
    while IFS= read -r ecfg; do
        while IFS= read -r location; do
        echo "Installing: $location"
        cp ${SCRIPTPATH}/$location ${HOME}/$location
        done < ${SCRIPTPATH}/.config_locations/$ecfg
    done < ${SCRIPTPATH}/.config_locations/enabled_configs
}

echo "Done"

while [ ! $# -eq 0 ]
do
    cd ${BASEDIR}
	case "$1" in
		-b | -backup | --b | --backup )
            backupDotFiles
        ;;
        -i | -install | --i | --install )
            installDotFiles
        ;;
        \? )
        echo "Usage: dotfiles [-b] [-i]"
        ;;
	esac
	shift
done

